//
//  NewsListVM.swift
//  NewsApp
//
//  Created by SHARON on 06/12/22.
//

import Foundation

protocol delegateNewsListVM: AnyObject {
    func successRetract(resultObj: TopHeadlinesResponseModel)
    func errorRetract(str_: String)
}

class NewsListVM {
    
    static let network = NetworkManager()
    static var getNewsResponse: TopHeadlinesResponseModel?
    static weak var delegateRetractCountVM: delegateNewsListVM?
    
    static func getNews(apiKey: String, country: String){
        var params = Parameters()
        params["apiKey"] = apiKey
        params["country"] = country
        network.getNews(params: params) { Response, error in
            if Response != nil {
                do {
                    let jsonData = try JSONSerialization.jsonObject(with: Response!, options: .mutableContainers)
                    print("Response: \n \(jsonData)")
                    let json = try JSONSerialization.jsonObject(with: Response!, options: []) as? [String : Any]
                    let responseModel = try JSONDecoder().decode(TopHeadlinesResponseModel.self, from: Response ?? Data())
                    self.getNewsResponse = responseModel
                    let status = responseModel.status ?? ""
                    DispatchQueue.main.async {
                        if status == "ok" {
                            self.delegateRetractCountVM?.successRetract(resultObj: responseModel)
                        } else {
                            self.delegateRetractCountVM?.errorRetract(str_: "error")
                        }
                    }
                }
                catch let error {
                    print(error)
                    DispatchQueue.main.async {
                        print("Error: \(error)")
                        self.delegateRetractCountVM?.errorRetract(str_: "error")
                    }
                }
            } else {
                if error != nil {
                    DispatchQueue.main.async {
                        let errMsg = error ?? "Error"
                        print("Error: \(errMsg)")
                        self.delegateRetractCountVM?.errorRetract(str_: "error")
                    }
                }
            }
        }
        
    }
    
}
