//
//  ViewController.swift
//  NewsApp
//
//  Created by SHARON on 06/12/22.
//

import UIKit

class ViewController: BasicViewController {
    
    @IBOutlet weak var newTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        apiCall()
        registerCell()
    }
    
    //MARK:- ConfigureCell
    func registerCell() {
        self.newTable.register(UINib(nibName: "NewsItemCell", bundle: nil), forCellReuseIdentifier: "NewsItemCell")
    }
    
    func apiCall() {
        NewsListVM.delegateRetractCountVM = self
        NewsListVM.getNews(apiKey: "d52c84fbbb514f5f946713cca3c19eeb", country: "us")
    }
    
    //MARK:- Actions
    func moveToArticle(index: Int) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ArticleViewController") as! ArticleViewController
        ArticleVM.articles = NewsListVM.getNewsResponse?.articles?[index]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:- Tableview Delegates & DataSource

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NewsListVM.getNewsResponse?.articles?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsItemCell", for: indexPath) as? NewsItemCell else {return UITableViewCell()}
        let data = NewsListVM.getNewsResponse?.articles?[indexPath.row]
        cell.dateLabel.text = Utilities.getDateTime(date: data?.publishedAt ?? "", format: "EEE - MMM dd, yyyy hh:mm a")
        cell.newsThumbImage.downloaded(from: URL(string: data?.urlToImage ?? "https://demofree.sirv.com/nope-not-here.jpg")!)
        cell.descriptionLabel.text = data?.description ?? ""
        cell.titleLabel.text = data?.title ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.moveToArticle(index: indexPath.row)
    }
}

//MARK:- WebserviceCall

extension ViewController: delegateNewsListVM {
    func successRetract(resultObj: TopHeadlinesResponseModel) {
        self.newTable.reloadData()
    }
    
    func errorRetract(str_: String) {
        self.newTable.reloadData()
    }
}
