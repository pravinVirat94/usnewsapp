//
//  ArticleViewController.swift
//  NewsApp
//
//  Created by SHARON on 07/12/22.
//

import UIKit

class ArticleViewController: BasicViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateAndAuthorLabel: UILabel!
    @IBOutlet weak var contentText: UITextView!
    @IBOutlet weak var newsThumbImage: UIImageView!
    @IBOutlet weak var backButton: UIButton!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- ConfigureUI
    
    func setupUI() {
        self.newsThumbImage.contentMode = .scaleAspectFill
        let newsData = ArticleVM.articles
        self.titleLabel.text = newsData?.title ?? ""
        self.contentText.text = newsData?.content ?? ""
        self.newsThumbImage.downloaded(from: URL(string: newsData?.urlToImage ?? "https://demofree.sirv.com/nope-not-here.jpg")!)
        self.backButton.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        self.dateAndAuthorLabel.text = "- " + (newsData?.author ?? "") + " " + (Utilities.getDateTime(date: newsData?.publishedAt ?? "", format: "EEE - MMM dd, yyyy hh:mm a"))
    }
    
    
    //MARK:- ButtonAction
    @objc func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
