


import Foundation
import UIKit

open class AppThemeColor {
    
    var blueThemeColour:UIColor{
        get{
            return UIColor("002868", alpha: 1.0)
        }
    }
    
    var redThemeColour:UIColor{
        get{
            return UIColor("80075", alpha: 1.0)
        }
    }
    
    var textColor: UIColor{
        get{
            return UIColor("FFFFFF", alpha: 1.0)
        }
    }
    
    var placeholderTextColor: UIColor{
        get{
            return UIColor("373030", alpha: 0.4)
        }
    }
    
    var fieldBackgroundColor: UIColor{
        get{
            return UIColor("D9D9D9", alpha: 0.5)
        }
    }
    
}

extension UIColor {
  
  convenience init(_ hex: String, alpha: CGFloat = 1.0) {
    var cString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if cString.hasPrefix("#") { cString.removeFirst() }
    
    if cString.count != 6 {
      self.init("ff0000") // return red color for wrong hex input
      return
    }
    
    var rgbValue: UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)
    
    self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
              green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
              blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
              alpha: alpha)
  }

}
