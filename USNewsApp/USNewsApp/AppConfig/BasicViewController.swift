

import UIKit
import Foundation

class BasicViewController: UIViewController {
    
    //let (blurView,activityIndicatorView) = Helper.getLoaderViews()
    let network = NetworkManager()
    let userDefaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.applyGradient()
        
    }
}

//Code for loader

/*extension BasicViewController {
    
    //Hider Animation
    func hideAnimation() {
     DispatchQueue.main.async {
     self.removeLoader(activityIndicatorView: self.activityIndicatorView, blurView: self.blurView)
     }
     }
     
     func showAnimation() {
     DispatchQueue.main.async {
     self.addLoaderToView(view:self.view,blurView:self.blurView ,activityIndicatorView:self.activityIndicatorView)
     }
     }
     
     func addLoaderToView(view:UIView,blurView:UIView ,activityIndicatorView:NVActivityIndicatorView) {
     
     blurView.isHidden = false
     blurView.frame = view.frame
     blurView.backgroundColor = UIColor.white.withAlphaComponent(0.6)
     view.addSubview(blurView)
     activityIndicatorView.center = blurView.center
     activityIndicatorView.color = AppThemeColor().redThemeColour
     view.addSubview(activityIndicatorView)
     activityIndicatorView.startAnimating()
     }
     
     func removeLoader(activityIndicatorView:NVActivityIndicatorView,blurView:UIView)  {
     activityIndicatorView.stopAnimating()
     blurView.isHidden = true
     }
     }
    
    
}*/

/*class Helper {
    
    Use for Loader
    static func getLoaderViews()->(UIView,NVActivityIndicatorView){
        let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 80, y: 80, width: 80, height:80), type: .triangleSkewSpin, color: AppThemeColor().blueThemeColour)
        let blurView = UIView()
        // create your components,customise and return
        return (blurView,activityIndicatorView)
    }
}*/
