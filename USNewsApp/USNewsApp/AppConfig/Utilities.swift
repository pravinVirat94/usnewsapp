


import Foundation
public class Utilities: NSObject {
    
    static let imageBaseURL = "https://ims-assets.wait-wise.com/"
    static func getDateTime(date: String, format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//"MM/dd/yy hh:mm:ss a"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        if let dt = dateFormatter.date(from: date) {
            dateFormatter.locale = Locale.current
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = format //"MMM dd, yyyy hh:mm a"
            return dateFormatter.string(from: dt)
        } else {
            return ""
        }
    }
}
