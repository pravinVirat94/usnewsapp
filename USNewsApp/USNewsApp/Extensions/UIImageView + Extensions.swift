//
//  UIImageView + Extensions.swift
//   
//
//  Created by SHARON D ROSE on 3/9/22.
//

import Foundation
import UIKit

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    
    func downloaded(from link: String, contentMode mode: ContentMode = .scaleAspectFit) {
        let urlString = link.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "https://www.eatgreenearth.com/wp-content/themes/eatgreen/images/no-image.jpg"
        guard let url = URL(string: urlString) else { return }
        downloaded(from: url, contentMode: mode)
    }
    func setImageTintColor(_ color: UIColor) {
        let tintedImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = tintedImage
        self.tintColor = color
      }
}
