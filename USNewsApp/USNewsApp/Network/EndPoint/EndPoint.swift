//
//  EndPoint.swift
//  NetworkLayer
//
//  Created by Malcolm Kumwenda on 2018/03/07.
//  Copyright © 2018 Malcolm Kumwenda. All rights reserved.
//

import Foundation
import SwiftUI

enum NetworkEnvironment {
    case qa
    case production
    case staging
}

public enum NetworkAPI {
    case getNewsList(params: Parameters)
}

extension NetworkAPI: EndPointType {
    var environmentBaseURL : String {
        switch NetworkManager.environment {
        case .production:
            return "https://newsapi.org/v2"
        case .qa:
            return ""
        case .staging:
            return ""
        }
    }

    var httpMethod: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    
    var path: String {
        switch self {
        case .getNewsList(_):
            return "/top-headlines"
        }
    }
    
    var task: HTTPTask {
        switch self {
        
        case .getNewsList(let params):
                let bodyEncoding: ParameterEncoding = .urlEncoding
                return .requestParameters(bodyParameters: nil, bodyEncoding: bodyEncoding, urlParameters: params)

       
        default:
            return .request
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}


